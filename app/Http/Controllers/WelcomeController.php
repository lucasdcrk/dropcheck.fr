<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pdp\Cache;
use Pdp\CurlHttpClient;
use Pdp\Manager;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function post(Request $request)
    {
        $request->validate([
            'website' => 'required|url'
        ]);

        $url = parse_url($request->website);
        $manager = new Manager(new Cache(), new CurlHttpClient());
        $rules = $manager->getRules();
        $domain = $rules->resolve($url['host'])->getRegistrableDomain();

        if (!isset($domain)) {
            return back()->withErrors(["L'URL saisie semble incorrect."]);
        }

        return redirect(route('websites.show', [$domain]));
    }
}
