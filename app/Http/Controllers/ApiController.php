<?php

namespace App\Http\Controllers;

use App\Models\Website;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function fqdn(Request $request)
    {
        $website = Website::where('domain', $request->domain)->first();
        abort_unless($website, 404);

        return response()->json([
            'name' => $website->name,
            'domain' => $website->domain,
            'logo' => $website->logo,
            'safe' => $website->safe
        ]);
    }
}
