<?php

namespace App\Http\Controllers;

use App\Models\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function show(Request $request)
    {
        $domain = $request->domain;
        $website = Website::where('domain', $domain)->first();

        if (!$website) {
            return view('websites.notfound', ['domain' => $domain]);
        }

        return view('websites.show', ['website' => $website]);
    }
}
