<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\WebsiteRequest as StoreRequest;
use App\Http\Requests\WebsiteRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class WebsiteCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class WebsiteCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Website');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/website');
        $this->crud->setEntityNameStrings('site web', 'sites web');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'label' => 'Nom du site',
            'name' => 'name'
        ]);

        $this->crud->addColumn([
            'label' => 'Nom de domaine',
            'name' => 'domain'
        ]);

        $this->crud->addColumn([
            'label' => 'Logo',
            'name' => 'logo',
            'type' => 'image',
            'prefix' => 'storage/',
            'height' => '100px'
        ]);

        $this->crud->addColumn([
            'label' => 'Safe ?',
            'name' => 'safe',
            'type' => 'check'
        ]);

        $this->crud->addColumn([
            'label' => 'Vérifié par',
            'name' => 'author_id',
            'type' => 'select',
            'entity' => 'author',
            'attribute' => 'name',
            'model' => "App\User"
        ]);

        $this->crud->addColumn([
            'label' => 'Ajouté le',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);

        $this->crud->addColumn([
            'label' => 'Mis à jour le',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([
            'label' => 'Nom du site',
            'name' => 'name'
        ]);

        $this->crud->addField([
            'label' => 'Nom de domaine  (sans www ni https://)',
            'name' => 'domain',
        ]);

        $this->crud->addField([
            'label' => 'Logo',
            'name' => 'logo',
            'type' => 'image',
            'prefix' => 'storage/',
            'height' => '100px'
        ]);

        $this->crud->addField([
            'label' => 'Safe ?',
            'name' => 'safe',
            'type' => 'checkbox'
        ]);

        $this->crud->addField([
            'label' => 'Rapport',
            'name' => 'description',
            'type' => 'summernote'
        ]);

        // add asterisk for fields that are required in WebsiteRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $request->request->set('author_id', backpack_user()->id);

        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
