<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::post('/', 'WelcomeController@post');

Route::view('api', 'api')->name('api');

Route::prefix('d')->group(function () {
    Route::prefix('{domain}')->group(function () {
        Route::get('/', 'WebsiteController@show')->name('websites.show');
    });
});