@extends('layouts.app')

@section('title', 'API')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="jumbotron">
                    <h1>API</h1>
                    <p class="lead">Une API sans authentification est à disposition si vous souhaitez intégrer {{ config('app.name') }}, elle a subjecte a changements donc elle est déconseillée en production.</p>
                    <h3>Endpoint :</h3>
                    <code>
                        {{ config('app.url') }}/api/fqdn/{nom de domaine}
                    </code>
                    <p class="mt-2">Aucune limitation du nombre requêtes n'est en vigueur pour le moment, merci de ne pas abuser du service.</p>
                    <hr>
                    <h3>Exemple :</h3>
                    <h6>Requête :</h6>
                    <pre>GET {{ config('app.url') }}/api/fqdn/produits-apple-pas-cher.fr</pre>
                    <h6>Réponse :</h6>
                    <pre>{"name":"Boutique Produits Apple", "domain":"produits-apple-pas-cher.fr", "logo":null, "safe":"1"}</pre>
                </div>
            </div>
        </div>
    </div>
@endsection