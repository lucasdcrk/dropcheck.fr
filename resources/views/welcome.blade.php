@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="jumbotron">
                    <h1>Bienvenue sur {{ config('app.name') }} !</h1>
                    <p class="lead">{{ config('app.name') }} est un site à but non lucratif qui vous permet de vérifier en 2 clicks si un site est connu pour la pratique du <a href="https://www.youtube.com/watch?v=Kb13pjN0DKw" target="_blank">drop shipping</a>. Les vérifications sont faites par des humains comme vous et moi afin d'en assurer la véracité.</p>
                    <hr class="my-4">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="url">URL du site</label>
                            <div class="input-group">
                                <input id="url" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }} form-control-lg" type="text" name="website" value="{{ old('website') }}" placeholder="https://www.boutiquenike.fr" required>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary">Rechercher</button>
                                </div>
                                @if ($errors->has('website'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection