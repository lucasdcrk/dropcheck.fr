@extends('layouts.app')

@section('title', $website->name)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-center">
                    <h1 class="display-4">{{ $website->name }}</h1>
                    <p class="lead">{{ $website->domain }}</p>
                </div>
                <hr>
                @if($website->safe)
                    <h1 class="display-4 text-center text-success"><i class="far fa-check-circle"></i> Honnête</h1>
                    <div class="alert alert-success mt-3" role="alert">
                        <h4 class="alert-heading">Ce site a été évalué comme honnête</h4>
                        <p><em>{{ $website->name }}</em> ne semble pas pratiquer le dropshipping, la vérification a été effectuée le <strong>{{ $website->updated_at->format('d/m/Y') }}</strong> par un membre de l'équipe.</p>
                    </div>
                @else
                    <h1 class="display-4 text-center text-danger"><i class="far fa-times-circle"></i> Malhonnête</h1>
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Ce site a été évalué comme malhonnête</h4>
                        <p>
                            <em>{{ $website->name }}</em> semble pratiquer le dropshipping, nous vous déconseillons d'effectuer des achats sur ce site.
                            Si vous ignorez ce qu'est le dropshipping vous trouverez une vidéo explicative
                            <a href="https://www.youtube.com/watch?v=Kb13pjN0DKw">en cliquant ici</a>.
                        </p>
                        <p>La vérification a été effectuée le <strong>{{ $website->updated_at->format('d/m/Y') }}</strong> par un membre de l'équipe.</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($website->description)
                <div class="card mb-3">
                    <div class="card-header">
                        Rapport
                    </div>
                    <div class="card-body">
                        {!! $website->description !!}
                        <div class="mt-5">
                            <i>Vérifié par {{ $website->author->name }} le {{ $website->updated_at->format('d/m/Y') }}</i>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header">
                        Commentaires
                    </div>
                    <div class="card-body">
                        @component('components.disqus')
                            @slot('identifier', $website->domain)
                        @endcomponent
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-header">
                        A propos
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-md-7">
                                <p>
                                    Ces données sont fournies à titre informatif.
                                    Si vous pensez que votre site a été catégorisé incorrectement contactez nous en précisant cette référence : <code class="text-uppercase">W{{ substr($website->domain, 0, 3) }}{{ $website->id }}</code>.
                                </p>
                            </div>
                            <div class="col-md-4">
                                <ul class="list-unstyled">
                                    <li>REF : <span class="float-right text-uppercase">W{{ substr($website->domain, 0, 3) }}{{ $website->id }}</span></li>
                                    <li>Ajouté le : <span class="float-right">{{ $website->created_at->format('d/m/Y') }}</span></li>
                                    <li>Mis à jour le : <span class="float-right">{{ $website->updated_at->format('d/m/Y') }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection