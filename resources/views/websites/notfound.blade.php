@extends('layouts.app')

@section('title', $domain)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-center">
                    <h1 class="display-4">{{ $domain }}</h1>
                    <p class="lead">Site web non répertorié</p>
                </div>
                <hr>
                <h1 class="display-4 text-center text-warning"><i class="far fa-question-circle"></i> Inconnu</h1>
                <div class="alert alert-warning" role="alert">
                    <h4 class="alert-heading">Ce site n'est pas encore répertorié</h4>
                    <p>Nous n'avons pas encore vérifié la légitimité de ce site, nous vous conseillons d'attendre.</p>
                    <p>Vous pouvez demander une vérification de ce site par nos équipes en passant sur notre serveur Discord ou en laissant un commentaire en dessous.</p>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header">
                        Commentaires
                    </div>
                    <div class="card-body">
                        @component('components.disqus')
                            @slot('identifier', $domain)
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection